#!/usr/bin/env python3
import os
import sys
import time
import subprocess
import shutil

class FactorioManager:
    def __init__(self):
        self.FACTORIO_SOCKET = "/run/factorio.stdin"
        self.FACTORIO_VERSION = "1.1.107"
        self.FACTORIO_URL = f"https://www.factorio.com/get-download/{self.FACTORIO_VERSION}/headless/linux64"
        self.FACTORIO_ARCHIVE = "linux64.tar.xz"
        self.CONFIG_FILE = "./data/server-settings.json"
        self.SAVES_FILE = "./saves/nanamix.zip"

    def run_command(self, command):
        try:
            subprocess.run(command, check=True)
        except subprocess.CalledProcessError as e:
            print(f'Error running Factorio server: {str(e)}')

    def start_factorio_server(self):
        self.run_command(["./bin/x64/factorio", "--start-server", self.SAVES_FILE, "--server-settings", self.CONFIG_FILE])

    def restart_factorio_server(self):
        self.run_command(["sudo", "systemctl", "restart", "factorio"])

    def stop_factorio_server(self):
        self.run_command(["sudo", "systemctl", "stop", "factorio"])

    def send_commands_to_factorio(self, command):
        with open(self.FACTORIO_SOCKET, "w") as file:
            file.write(command + "\n")

    def update_factorio(self):
        #download factorio update
        self.send_commands_to_factorio("/quit")
        self.stop_factorio_server()
        self.run_command(["wget", self.FACTORIO_URL, "-O", self.FACTORIO_ARCHIVE])
        self.run_command(["tar", "-xvf", self.FACTORIO_ARCHIVE])
        #copy old config file
        self.run_command(["cp", self.CONFIG_FILE, "./factorio/data/"])

        #delete old stuff
        path_to_remove = ['./bin', './data', './config-path.cfg']
        for path in path_to_remove:
            self.remove_path(path)

        #replace server by new one
        self.run_command(["mv", "./factorio/*", "./"])
        print(f"File moved successfully from './factorio/*' to './' ")
        self.remove_path('./factorio')
        time.sleep(2)

        #relaunch server
        self.restart_factorio_server()

        time.sleep(6)
        self.send_commands_to_factorio("nanami factorio")
        print(f'-- FACTORIO UPDATED - ALL DONE --')

    # BEWARE HERE BE DRAGONS
    # shutil.rmtree works the same way as 'rm -rf', be careful with this part
    def remove_path(self, path):
        if os.path.isdir(path):
            shutil.rmtree(path)
            print(f'Folder {path} and its contents have been deleted.')
        else:
            print(f'Folder {path} does not exist.')

if __name__ == '__main__':
    # read socket created by systemd
    #command = sys.stdin.readline().strip()

    factorio_manager = FactorioManager()
    factorio_manager.start_factorio_server()
