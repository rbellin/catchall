#!/usr/bin/env sh

#download factorio update
echo "/quit" > /run/factorio.stdin
sudo systemctl stop factorio
wget https://www.factorio.com/get-download/1.1.107/headless/linux64 -O linux64.tar.xz
tar -xvf linux64.tar.xz

#copy old config file
cp ./data/server-settings.json ./factorio/data/

#delete old stuff
rm -rf ./bin
rm -rf ./data
rm -rf ./config-path.cfg

#replace server by new one
mv ./factorio/* ./
rm -rf ./factorio

sleep 2
#relaunch server
sudo systemctl restart factorio
sleep 6
echo "nanami factorio" > /run/factorio.stdin

echo "-- FACTORIO UPDATED - ALL DONE --"
